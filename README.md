**Hobbystyczny projekt stacji pogodowej w Pacynie**

Dane są publicznie dostępne na: https://www.pogodapacyna.pl

## Do zrobienia:

1. Usunięcie elementów w iframe (?)
2. Skrypt włączający język polski na start, aktualnie trzeba to zrobić ręcznie, instrukcja: https://pogodapacyna.pl/instrukcja.html
3. Dodanie informacji o zanieczyszczeniach wykorzystując API - https://developer.airly.eu/docs

---

## Co ostatnio zostało zrobione:

1. Podpięty i wymuszony SSL
2. Detekcja w JS mobile/desktop i ustawianie wygodniejszej rozdzielczości do podglądu
3. Dodane dane techniczne